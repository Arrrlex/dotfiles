#!/usr/bin/env zsh

# (Nothing above this line)
# Prompt setup (powerlevel 10k)
source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"

# Local setup
#  - Set one of IS_WSL, IS_MAC, IS_LINUX
#  - If miniconda installed, set CONDA_PATH
source ~/.zsh_local

# OS-specific setup
[[ -v IS_WSL ]] && source ~/.zsh_wsl
[[ -v IS_MAC ]] && source ~/.zsh_mac
[[ -v IS_LINUX ]] && source ~/.zsh_linux

# Aliases
source ~/.zsh_aliases

# General setup
export LANG=en_US.UTF-8
export PATH="$HOME/.local/bin:/usr/local/sbin:$PATH"

# oh-my-zsh setup
export ZSH="$HOME/.oh-my-zsh"
export NVM_LAZY_LOAD=true NVM_COMPLETION=true
ZSH_THEME="powerlevel10k/powerlevel10k"
plugins=(evalcache gitfast zsh-nvm z)
source $ZSH/oh-my-zsh.sh

# sensible defaults for less
export LESS='--no-init --quit-if-one-screen -R'

# pyenv
export PATH="$HOME/.pyenv/bin:$PATH"
_evalcache pyenv init - zsh

# conda / mamba
[[ -v CONDA_PATH ]] && source $CONDA_PATH/etc/profile.d/conda.sh
[[ -v MAMBA_PATH ]] && source $MAMBA_PATH/etc/profile.d/mamba.sh

# npm
[[ -d ~/.npm-global ]] && export PATH=~/.npm-global/bin:$PATH

# sdkman
[[ -d ~/.sdkman ]] && source ~/.sdkman/bin/sdkman-init.sh

# fuck
# _evalcache thefuck --alias

# go
export GOPATH=~/go
export PATH=$PATH:$GOPATH/bin
export GOPRIVATE="github.com/sumup/*"

source ~/.p10k.zsh
# Nothing below this line
